/**
 * Single account widget
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QWidget>

#include "account.h"

namespace Ui {
class AccountWidget;
}

class AccountWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AccountWidget(const Account &account, QWidget *parent = 0);
    ~AccountWidget();


    Account account() const;


public slots:
    void deleteMovement(const Movement &m);

signals:
    void updated(const Account &account);
    void deleteAccount(const Account &account);

private slots:

    void on_renameButton_clicked();

    void on_deleteButton_clicked();

    void resetCreateForm();

    void submitCreateForm();

    void on_insertButton_clicked();

    void on_name_returnPressed();

    void refreshUI();

    void checkForUpdate(int row, int column);

    void cellChanged(int, int, int previousRow, int previousColumn);

    void openTableContextMenu(const QPoint &pos);
    
private:

    // Class members
    Ui::AccountWidget *ui;
    Account mAccount;
};
