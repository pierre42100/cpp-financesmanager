#include <QDate>
#include <QChart>
#include <QChartView>
#include <QLineSeries>
#include <QValueAxis>
#include <QBarCategoryAxis>

#include "account.h"
#include "accountscharts.h"
#include "ui_accountscharts.h"

using namespace QtCharts;

AccountsCharts::AccountsCharts(const QList<Account> &accounts, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AccountsCharts)
{
    ui->setupUi(this);

    if(accounts.count() == 0)
        return; // Can not do anything

    // Determine minimum and maximum date
    QDate minDate, maxDate;

    for(auto a : accounts) {

        auto currMin = a.minDate();
        auto currMax = a.maxDate();

        if(!minDate.isValid() || minDate > currMin)
            minDate = currMin;

        if(!maxDate.isValid() || maxDate < currMax)
            maxDate = currMax;
    }

    auto chart = new QChart();

    auto min = 0;
    auto max = 0;

    // Create the series of each account
    for(auto a : accounts) {
        auto series = new QLineSeries();
        series->setName(a.name());

        auto currDate = minDate;
        double balance = 0;
        for(int i = 0; currDate <= maxDate; i++, currDate = currDate.addDays(1)) {
            balance += a.balanceOfDay(currDate);
            *series << QPointF(i, balance);

            if(balance - 10 < min)
                min = balance - 10;

            if(balance + 10 > max)
                max = balance + 10;
        }

        chart->addSeries(series);
    }


    // X axis
    QStringList categories;
    for(auto currDate = minDate; currDate <= maxDate; currDate = currDate.addDays(1))
        categories << currDate.toString("dd-MM-yy");
    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    axisX->setVisible(true);
    chart->addAxis(axisX, Qt::AlignBottom);

    // Y axis
    auto axisY = new QValueAxis();
    axisY->setRange(min, max);
    axisY->setTickCount(10);
    chart->addAxis(axisY, Qt::AlignLeft);
    chart->series().at(0)->attachAxis(axisY);



    chart->setTheme(QChart::ChartThemeDark);

    auto chartView = new QChartView(chart);
    ui->verticalLayout->addWidget(chartView);
}

AccountsCharts::~AccountsCharts()
{
    delete ui;
}
