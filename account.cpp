#include <algorithm>

#include <QDateTime>

#include "account.h"

static int currID = 0;

Account::Account()
{
    this->mId = currID;
    currID++;
}

int Account::id() const
{
    return mId;
}


QString Account::name() const
{
    return mName;
}

void Account::setName(const QString &name)
{
    mName = name;
}

QList<Movement> Account::movements() const
{
    return mMovements;
}

void Account::addMovement(const Movement &movement)
{
    mMovements.append(movement);
    sortMovements();
}

void Account::deleteMovement(const Movement &movement)
{
    mMovements.removeAll(movement);
}

void Account::updateMovement(const Movement &newMovement)
{
    mMovements.replace(mMovements.indexOf(newMovement), newMovement);
    sortMovements();
}

void Account::setMovements(const QList<Movement> &movements)
{
    mMovements = movements;
}

double Account::balance() const
{
    double amount = 0;
    for(auto m : mMovements)
        amount += m.amount();
    return amount;
}

QDate Account::minDate() const
{
    if(mMovements.count() == 0)
        return QDateTime::currentDateTime().date();

    auto date = mMovements[0].date();

    for(auto m : mMovements)
        if(date > m.date())
            date = m.date();

    return date;
}

QDate Account::maxDate() const
{
    if(mMovements.count() == 0)
        return QDateTime::currentDateTime().date();

    auto date = mMovements[0].date();

    for(auto m : mMovements)
        if(date < m.date())
            date = m.date();

    return date;
}

double Account::balanceOfDay(const QDate &day) const
{
    double balance = 0;

    for(auto m : mMovements) {
        if(m.date() == day)
            balance += m.amount();
    }

    return balance;
}

bool Account::operator==(const Account &other)
{
    return id() == other.id();
}

void Account::sortMovements()
{
    std::sort(mMovements.begin(), mMovements.end());
}
