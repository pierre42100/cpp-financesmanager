#include <QList>
#include <QBuffer>
#include <QFile>

#include "serializationhelper.h"
#include "account.h"

SerializationHelper::SerializationHelper()
{

}

QList<Account> SerializationHelper::Load(const QString &path)
{
    QList<Account> l;

    QFile file(path);

    if(!file.exists()) {
        return l;
    }

    if(!file.open(QFile::ReadOnly)) {
        qFatal("Could not open file!");
    }

    while(!file.atEnd()) {

        QString line = file.readLine();

        if(line.isEmpty() || line == "\n")
            continue;

        // Set account name
        Account a;
        a.setName(line.replace('\n', ""));

        // Skip next line
        file.readLine();

        while(true){
            line = file.readLine();

            if(line.isEmpty() || line == "\n")
                break;

            auto infoSplit = line.split(";");

            if(infoSplit.count() != 3) continue;

            a.addMovement(Movement(infoSplit[0].toInt(),
                            infoSplit[1],
                            infoSplit[2].toDouble()));
        }

        l.append(a);
    }


    file.close();


    return l;
}

bool SerializationHelper::Serialize(const QString &path, const QList<Account> &accounts)
{
    QString data;

    for(auto account : accounts) {
        data += account.name() + "\n==============\n";

        for(auto movement : account.movements())
            data += QString::number(movement.time()) + ";" + movement.label().replace(';', ',') + ";" + QString::number(movement.amount())+"\n";

        data += "\n\n\n";
    }

    QFile file(path);

    if(!file.open(QFile::WriteOnly))
        return false;

    file.write(data.toUtf8());
    if(!file.flush())
        return false;
    file.close();

    return true;
}
