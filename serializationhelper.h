/**
 * Accounts serialization helper
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QString>

class Account;

class SerializationHelper
{
public:
    SerializationHelper();

    static QList<Account> Load(const QString &path);

    static bool Serialize(const QString &path, const QList<Account> &accounts);
};
