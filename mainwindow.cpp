#include <QMessageBox>
#include <QInputDialog>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "serializationhelper.h"
#include "accountwidget.h"
#include "accountscharts.h"

MainWindow::MainWindow(const QString &filePath, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mFilePath(filePath)
{
    ui->setupUi(this);

    // Load accounts
    this->mAccounts = SerializationHelper::Load(mFilePath);

    for(auto account : mAccounts)
        addAccountTab(account);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openCharts()
{
    AccountsCharts *widget = new AccountsCharts(mAccounts);
    widget->show();
}

void MainWindow::addNewAccount()
{
    QString newName = QInputDialog::getText(this,
                          tr("Add a new account"),
                          tr("Please enter the name of the new account:"));

    if(newName.isEmpty())
        return;

    // Create the account
    Account account;
    account.setName(newName);
    this->mAccounts.append(account);

    addAccountTab(account);
}

void MainWindow::updatedAccount(const Account &account)
{
    // Update tab name
    ui->tabWidget->setTabText(getAccountTabNumber(account), account.name());

    // Change the account in the list
    this->mAccounts.replace(mAccounts.indexOf(account), account);

    // Save the new list
    save();
}

void MainWindow::deleteAccount(const Account &account)
{
    ui->tabWidget->setCurrentIndex(0);

    // Remove account widget
    auto index = getAccountTabNumber(account);
    ui->tabWidget->widget(index)->deleteLater();
    ui->tabWidget->removeTab(index);

    // Remove account from the list
    mAccounts.removeAll(account);

    // Save the new list
    save();
}

void MainWindow::on_actionQuit_triggered()
{
    QCoreApplication::quit();
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    QApplication::aboutQt();
}

void MainWindow::on_actionAbout_FinancesManager_triggered()
{
    QMessageBox::information(this,
                             tr("About FinancesManager"),
                             tr("(c) Pierre HUBERT 2019. MIT License"));
}

void MainWindow::addAccountTab(const Account &account)
{
    AccountWidget *accountWidget = new AccountWidget(account);

    connect(accountWidget, &AccountWidget::updated, this, &MainWindow::updatedAccount);
    connect(accountWidget, &AccountWidget::deleteAccount, this, &MainWindow::deleteAccount);

    // Create the new tab and activate it
    auto newIndex = ui->tabWidget->count() - 1;
    ui->tabWidget->insertTab(newIndex, accountWidget, account.name());
    ui->tabWidget->setCurrentIndex(newIndex);
}

int MainWindow::getAccountTabNumber(const Account &account)
{
    for(int i = 0; i < ui->tabWidget->count() - 1; i++) {
        if(qobject_cast<AccountWidget*>(ui->tabWidget->widget(i))->account().id() == account.id())
            return i;
    }

    throw "Could not find account!";
}

void MainWindow::save()
{
    if(!SerializationHelper::Serialize(mFilePath, mAccounts))
        QMessageBox::warning(this,
                             tr("Save failed"),
                             tr("Could not save new list!"));
}

void MainWindow::on_actionNew_Account_triggered()
{
    addNewAccount();
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if(index == ui->tabWidget->count() - 1)
        addNewAccount();
}

void MainWindow::on_actionCharts_triggered()
{
    openCharts();
}
