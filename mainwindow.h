#pragma once

#include <QMainWindow>

#include "account.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const QString &filePath, QWidget *parent = 0);
    ~MainWindow();

    void openCharts();

private slots:

    void addNewAccount();

    void updatedAccount(const Account &account);

    void deleteAccount(const Account &account);

    void on_actionQuit_triggered();

    void on_actionAbout_Qt_triggered();

    void on_actionAbout_FinancesManager_triggered();

    void on_actionNew_Account_triggered();

    void on_tabWidget_currentChanged(int index);

    void on_actionCharts_triggered();

private:

    void addAccountTab(const Account &account);

    int getAccountTabNumber(const Account &account);

    void save();

    // Class members
    Ui::MainWindow *ui;
    QString mFilePath;
    QList<Account> mAccounts;
};
