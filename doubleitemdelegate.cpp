#include <QTableWidget>
#include <QDoubleSpinBox>

#include "doubleitemdelegate.h"

DoubleItemDelegate::DoubleItemDelegate(QTableWidget *parent) :
    QItemDelegate(), mTable(parent)
{

}

QWidget *DoubleItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    auto w = new QDoubleSpinBox(parent);
    w->setMinimum(-99999999999);
    w->setMaximum(999999999999);

    connect(w, &QDoubleSpinBox::destroyed, [this, index](){
        emit this->checkForUpdate(index.row(), index.column());
    });

    return w;
}

void DoubleItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    auto w = qobject_cast<QDoubleSpinBox*>(editor);

    w->setValue(mTable->item(index.row(), index.column())->text().toDouble());
}
