#include <QDateTime>
#include <QInputDialog>
#include <QMessageBox>
#include <QMenu>

#include "accountwidget.h"
#include "dateitemdelegate.h"
#include "doubleitemdelegate.h"
#include "ui_accountwidget.h"

AccountWidget::AccountWidget(const Account &account, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AccountWidget), mAccount(account)
{
    ui->setupUi(this);
    resetCreateForm();

    connect(this, &AccountWidget::updated, this, &AccountWidget::refreshUI);

    connect(ui->tableWidget, &QTableWidget::currentCellChanged, this, &AccountWidget::cellChanged);

    // Set column names
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Date")));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Label")));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Amount")));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Total")));

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    auto dateItemDelegate = new DateItemDelegate(ui->tableWidget);
    connect(dateItemDelegate, &DateItemDelegate::checkForUpdate, this, &AccountWidget::checkForUpdate);
    ui->tableWidget->setItemDelegateForColumn(0, dateItemDelegate);

    auto doubleItemDelegate = new DoubleItemDelegate(ui->tableWidget);
    connect(doubleItemDelegate, &DoubleItemDelegate::checkForUpdate, this, &AccountWidget::checkForUpdate);
    ui->tableWidget->setItemDelegateForColumn(2, doubleItemDelegate);

    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableWidget, &QTableWidget::customContextMenuRequested, this, &AccountWidget::openTableContextMenu);

    refreshUI();
}

AccountWidget::~AccountWidget()
{
    delete ui;
}

void AccountWidget::on_renameButton_clicked()
{
    auto name = QInputDialog::getText(this,
                                      tr("Rename account"),
                                      tr("Please enter the new name for the account:"),
                                      QLineEdit::Normal,
                                      mAccount.name());

    if(name.isEmpty())
        return;

    mAccount.setName(name);

    emit updated(mAccount);
}

Account AccountWidget::account() const
{
    return mAccount;
}

void AccountWidget::deleteMovement(const Movement &m)
{
    if(QMessageBox::question(this,
                             tr("Delete movement"),
                             tr("Do you really want to this movement ? (%1)").arg(m.label()),
                             QMessageBox::Yes,
                             QMessageBox::No) != QMessageBox::Yes)
        return;

    mAccount.deleteMovement(m);

    emit updated(mAccount);
}

void AccountWidget::on_deleteButton_clicked()
{
    if(QMessageBox::question(this,
                             tr("Delete account"),
                             tr("Do you really want to delete this account ?"),
                             QMessageBox::Yes,
                             QMessageBox::No) != QMessageBox::Yes)
        return;

    emit deleteAccount(mAccount);
}

void AccountWidget::resetCreateForm()
{
    ui->date->setDate(QDate::currentDate());
    ui->name->setText("");
    ui->amount->setValue(0);
}

void AccountWidget::submitCreateForm()
{
    mAccount.addMovement(Movement(ui->date->dateTime().toSecsSinceEpoch(), ui->name->text(), ui->amount->value()));
    emit updated(mAccount);
    resetCreateForm();
}

void AccountWidget::on_insertButton_clicked()
{
    submitCreateForm();
}

void AccountWidget::on_name_returnPressed()
{
    submitCreateForm();
}

void AccountWidget::refreshUI()
{

    auto balance = mAccount.balance();
    ui->balance->setText(QString::number(balance));

    ui->balance->setStyleSheet(QString("* { color: ")+ (balance < 0 ? "red" : "green")+"; }");

    ui->tableWidget->setRowCount(mAccount.movements().count());
    double total = mAccount.balance();
    for(int i = 0; i < mAccount.movements().size(); i++) {
        auto movement = mAccount.movements()[i];

        // Set the new values
        ui->tableWidget->setItem(i, 0, new QTableWidgetItem(movement.date().toString("dd/MM/yyyy")));
        ui->tableWidget->setItem(i, 1, new QTableWidgetItem(movement.label()));
        ui->tableWidget->setItem(i, 2, new QTableWidgetItem(QString::number(movement.amount())));

        ui->tableWidget->item(i,2)->setForeground(QBrush(QColor(
                                                     movement.amount() > 0 ? 50 : 255,
                                                     movement.amount() > 0 ? 150 : 0,
                                                     50)));



        // Compute & show total
        auto totalItem = new QTableWidgetItem(QString::number(total));
        totalItem->setFlags(totalItem->flags() ^ Qt::ItemIsEditable);
        totalItem->setForeground(QBrush(QColor(total > 0 ? 50 : 255,
                                        total > 0 ? 150 : 0,
                                        50)));
        ui->tableWidget->setItem(i, 3, totalItem);
        total -= movement.amount();


        // Delete button
        auto deleteBtn = new QPushButton(tr("Delete"));
        connect(deleteBtn, &QPushButton::clicked, [this, movement]{
           this->deleteMovement(movement);
        });

        ui->tableWidget->setCellWidget(i, 4, deleteBtn);
    }
}

void AccountWidget::checkForUpdate(int row, int column)
{
    if(row >= mAccount.movements().count()) return;

    // Change date
    if(column == 0) {
        auto dateStr = ui->tableWidget->item(row, column)->text();

        // Check if the editor has not been opened
        if(dateStr.indexOf('/') != -1)
            return;

        auto dateSplit = dateStr.split("-");
        auto date = QDate();
        date.setDate(dateSplit[0].toInt(),
                    dateSplit[1].toInt(),
                    dateSplit[2].toInt()
                );

        auto m = mAccount.movements()[row];
        m.setDate(date);
        mAccount.updateMovement(m);
    }


    // Change label
    else if(column == 1) {
        auto m = mAccount.movements()[row];
        auto newLabel = ui->tableWidget->item(row, column)->data(Qt::EditRole).toString();

        if(m.label() == newLabel)
            return;

        m.setLabel(newLabel);
        mAccount.updateMovement(m);
    }

    // Change amount
    else if(column == 2) {
        auto m = mAccount.movements()[row];
        auto newAmount = ui->tableWidget->item(row, column)->text().toDouble();

        if(m.amount() == newAmount)
            return;

        m.setAmount(newAmount);
        mAccount.updateMovement(m);
    }

    qDebug("did update");
    emit updated(mAccount);
}

void AccountWidget::cellChanged(int, int, int previousRow, int previousColumn)
{
    if(previousColumn < 0 || previousColumn < 0 || ui->tableWidget->item(previousRow, previousColumn) == nullptr)
        return;


    checkForUpdate(previousRow, previousColumn);
}

void AccountWidget::openTableContextMenu(const QPoint &pos)
{
    auto row = ui->tableWidget->rowAt(pos.y());
    auto col = ui->tableWidget->columnAt(pos.x());

    if(row == -1 || col == -1) return;

    const auto m = mAccount.movements()[row];

    QMenu contextMenu(tr("Context menu"), this);

    QAction action1("Delete movement", this);
    connect(&action1, &QAction::triggered, [&]{
        this->deleteMovement(m);
    });
    contextMenu.addAction(&action1);

    contextMenu.exec(ui->tableWidget->mapToGlobal(pos));
}
