#pragma once

#include <QWidget>

class Account;

namespace Ui {
class AccountsCharts;
}

class AccountsCharts : public QWidget
{
    Q_OBJECT

public:
    explicit AccountsCharts(const QList<Account> &accounts, QWidget *parent = 0);
    ~AccountsCharts();

private:
    Ui::AccountsCharts *ui;
};
