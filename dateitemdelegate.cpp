#include <QTableWidget>
#include <QDateEdit>

#include "dateitemdelegate.h"

DateItemDelegate::DateItemDelegate(QTableWidget *parent)
    : QItemDelegate(parent), mTable(parent)
{

}

QWidget *DateItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const
{
    auto editor = new QDateEdit(parent);

    connect(editor, &QDateEdit::destroyed, [this, index](){
        emit this->checkForUpdate(index.row(), index.column());
    });

    return editor;
}

void DateItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    auto dateSplit = mTable->item(index.row(), index.column())->text().split("/");

    if(dateSplit.count() != 3)
        return;

    auto date = QDate();
    date.setDate(
                dateSplit[2].toInt(),
                dateSplit[1].toInt(),
                dateSplit[0].toInt()
                );


    // Update date
    auto el = qobject_cast<QDateEdit*>(editor);
    el->setDate(date);
}
