#include <QDate>
#include <QDateTime>

#include "movement.h"

static int totalSerial = 0;

Movement::Movement()
{

}

Movement::Movement(int time, const QString &label, double amount)
    : mSerial(totalSerial++), mTime(time), mLabel(label), mAmount(amount)
{

}

int Movement::time() const
{
    return mTime;
}

void Movement::setTime(int time)
{
    mTime = time;
}

void Movement::setDate(QDate date)
{
    mTime = date.startOfDay().toSecsSinceEpoch();
}

QDate Movement::date() const
{
    return QDateTime::fromSecsSinceEpoch(time()).date();
}

QString Movement::label() const
{
    return mLabel;
}

void Movement::setLabel(const QString &label)
{
    mLabel = label;
}

double Movement::amount() const
{
    return mAmount;
}

void Movement::setAmount(double amount)
{
    mAmount = amount;
}

bool Movement::operator!=(const Movement &other)
{
    return serial() != other.serial();
}

bool Movement::operator==(const Movement &other)
{
    return serial() == other.serial();
}


int Movement::serial() const
{
    return mSerial;
}

bool operator<(const Movement &one, const Movement &two)
{
    return one.time() != two.time() ? one.time()-two.time() > 0 : one.label() > two.label();
}
