/**
 * Main file
 *
 * @author Pierre HUBERT
 */

#include <iostream>

#include <QApplication>
#include <QTimer>
#include <QFileDialog>
#include <QMessageBox>

#include "mainwindow.h"

using namespace std;

int main(int argc, char **argv) {
    cout << "FinancesManager (c) Pierre HUBERT 2019" << endl;

    QApplication app(argc, argv);

    QTimer::singleShot(0, []{

        // Check if a file was specified in the command line
        QString filePath;
        auto args = QCoreApplication::arguments();
        if(args.count() > 1)
            filePath = args[1];

        else
            // Ask the user for the path to use
            filePath = QFileDialog::getSaveFileName(
                        nullptr,
                        QObject::tr("Choose the directory to open"),
                        QString(),
                        "*.finance",
                        Q_NULLPTR,
                        QFileDialog::DontConfirmOverwrite
                       );

        if(filePath.isEmpty()) {
            QMessageBox::critical(nullptr,
                                 QObject::tr("No file selected"),
                                 QObject::tr("Can not start without a file!"));
            return;
        }

        cout << "Starting now..." << endl;
        (new MainWindow(filePath))->show();
    });

    return app.exec();
}
