/**
 * Single financial movement
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QString>

class QDate;

class Movement
{
public:
    Movement();

    Movement(int time, const QString &label, double amount);

    int serial() const;

    int time() const;
    void setTime(int time);

    void setDate(QDate date);
    QDate date() const;

    QString label() const;
    void setLabel(const QString &label);

    double amount() const;
    void setAmount(double amount);

    bool operator!=(const Movement &other);

    bool operator==(const Movement &other);


private:
    int mSerial;
    int mTime;
    QString mLabel;
    double mAmount;
};

bool operator<(const Movement &one, const Movement &two);
