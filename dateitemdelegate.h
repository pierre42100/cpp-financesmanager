/**
 * This class is an helper to easily edit date in
 * QTableWidget
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QItemDelegate>

class QTableWidget;

class DateItemDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit DateItemDelegate(QTableWidget *parent = nullptr);

signals:
    void checkForUpdate(int row, int column) const;

protected:
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const override;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;


public slots:

private:
    QTableWidget *mTable;
};
