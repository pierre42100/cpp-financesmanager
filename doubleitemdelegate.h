#pragma once

#include <QItemDelegate>

class QTableWidget;

class DoubleItemDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit DoubleItemDelegate(QTableWidget *parent = nullptr);

signals:
    void checkForUpdate(int row, int column) const;

public slots:

protected:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const override;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;


private:
    QTableWidget *mTable;
};
