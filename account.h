/**
 * Single account information & operations
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <QList>

#include "movement.h"

class Account
{
public:
    Account();

    int id() const;

    QString name() const;
    void setName(const QString &name);

    QList<Movement> movements() const;
    void addMovement(const Movement &movement);
    void deleteMovement(const Movement &movement);
    void updateMovement(const Movement &newMovement);
    void setMovements(const QList<Movement> &movements);

    double balance() const;

    QDate minDate() const;

    QDate maxDate() const;

    double balanceOfDay(const QDate &day) const;

    bool operator==(const Account &other);

private:
    void sortMovements();

    // Class members
    int mId;
    QString mName;
    QList<Movement> mMovements;
};
