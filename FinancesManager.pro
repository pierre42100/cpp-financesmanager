QT += widgets charts

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    movement.cpp \
    account.cpp \
    serializationhelper.cpp \
    accountwidget.cpp \
    dateitemdelegate.cpp \
    doubleitemdelegate.cpp \
    accountscharts.cpp

FORMS += \
    mainwindow.ui \
    accountwidget.ui \
    accountscharts.ui

HEADERS += \
    mainwindow.h \
    movement.h \
    account.h \
    serializationhelper.h \
    accountwidget.h \
    dateitemdelegate.h \
    doubleitemdelegate.h \
    accountscharts.h
